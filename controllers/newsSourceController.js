const NewsSource = require("../models/newsSourceModel");
const News = require("../models/newsModel");

/**
 * Create a news source
 *
 * @param {*} req
 * @param {*} res
 */
const newsSourcePost = (req, res) => {
  var newsSource = new NewsSource();

  newsSource.url = req.body.url;
  newsSource.nombre = req.body.nombre;
  newsSource.id_categoria = req.body.id_categoria;
  newsSource.id_usuario = req.body.id_usuario;

  if (newsSource.url && newsSource.nombre && newsSource.id_categoria && newsSource.id_usuario) {
    newsSource.save(function (err) {
      if (err) {
        res.status(422);
        console.log('error while saving the newsSource', err)
        res.json({
          error: 'There was an error saving the newsSource'
        });
      }
      res.status(201);
      res.header({
        'location': `http://localhost:3000/api/newsSources/?id=${newsSource.id}`
      });
      res.json(newsSource);
    });
  } else {
    res.status(422);
    console.log('error while saving the newsSource')
    res.json({
      error: 'No valid data provided for newsSource'
    });
  }
};

/**
 * Get one or all news sources
 *
 * @param {*} req
 * @param {*} res
 */
const newsSourceGet = (req, res) => {
  if (req.query && req.query.id) {
    NewsSource.findById(req.query.id, function (err, newsSource) {
      if (err) {
        res.status(404);
        console.log('error while queryting the newsSource', err)
        res.json({ error: "NewsSource doesnt exist" })
      }
      res.json(newsSource);
    });
  } else {
    NewsSource.find(function (err, newsSource) {
      if (err) {
        res.status(422);
        res.json({ "error": err });
      }
      res.json(newsSource);
    });

  }
};

/**
 * Delete a news source
 *
 * @param {*} req
 * @param {*} res
 */
const newsSourceDelete = (req, res) => {
  if (req.query && req.query.id) {
    NewsSource.findById(req.query.id, function (err, newsSource) {
      if (err) {
        res.status(500);
        console.log('error while queryting the newsSource', err)
        res.json({ error: "NewsSource doesnt exist" })
      }
      if (newsSource) {
        newsSource.remove(function (err) {
          if (err) {
            res.status(500).json({ message: "There was an error deleting the newsSource" });
          }
          res.status(204).json({});
        })
      } else {
        res.status(404);
        console.log('error while queryting the newsSource', err)
        res.json({ error: "NewsSource doesnt exist" })
      }
    });
  } else {
    res.status(404).json({ error: "You must provide a NewsSource ID" });
  }
};

/**
 * Update a news source
 *
 * @param {*} req
 * @param {*} res
 */
const newsSourcePatch = (req, res) => {
  if (req.query && req.query.id) {
    NewsSource.findById(req.query.id, function (err, newsSource) {
      if (err) {
        res.status(404);
        console.log('error while queryting the newsSource', err)
        res.json({ error: "NewsSource doesnt exist" })
      }

      newsSource.url = req.body.url ? req.body.url : task.url;
      newsSource.nombre = req.body.nombre ? req.body.nombre : task.nombre;
      newsSource.id_categoria = req.body.id_categoria ? req.body.id_categoria : task.id_categoria;
      newsSource.id_usuario = req.body.id_usuario ? req.body.id_usuario : task.id_usuario;

      newsSource.save(function (err) {
        if (err) {
          res.status(422);
          console.log('error while saving the newsSource', err)
          res.json({
            error: 'There was an error saving the newsSource'
          });
        }
        res.status(200);
        res.json(newsSource);
      });
    });
  } else {
    res.status(404);
    res.json({ error: "NewsSource doesnt exist" })
  }
};

module.exports = {
  newsSourceGet,
  newsSourcePost,
  newsSourcePatch,
  newsSourceDelete
}