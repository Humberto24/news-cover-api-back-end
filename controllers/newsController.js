const News = require("../models/newsModel");

/**
 * Create a news
 *
 * @param {*} req
 * @param {*} res
 */
const newsPost = (req, res) => {
  var news = new News();

  news.titulo = req.body.titulo;
  news.descripcion = req.body.descripcion;
  news.link = req.body.link;
  news.fecha = req.body.fecha;
  news.id_fuente_noticia = req.body.id_fuente_noticia;
  news.id_usuario = req.body.id_usuario;
  news.id_categoria = req.body.id_categoria;


  if (news.titulo && news.descripcion && news.link && news.fecha && news.id_fuente_noticia
    && news.id_usuario && news.id_categoria) {
    news.save(function (err) {
      if (err) {
        res.status(422);
        console.log('error while saving the news', err)
        res.json({
          error: 'There was an error saving the news'
        });
      }
      res.status(201);
      res.header({
        'location': `http://localhost:3000/api/news/?id=${news.id}`
      });
      res.json(news);
    });
  } else {
    res.status(422);
    console.log('error while saving the news')
    res.json({
      error: 'No valid data provided for news'
    });
  }
};

/**
 * Get one or all news
 *
 * @param {*} req
 * @param {*} res
 */
const newsGet = (req, res) => {
  if (req.query && req.query.id) {
    News.findById(req.query.id, function (err, news) {
      if (err) {
        res.status(404);
        console.log('error while queryting the news', err)
        res.json({ error: "News doesnt exist" })
      }
      res.json(news);
    });
  } else {
    News.find(function (err, news) {
      if (err) {
        res.status(422);
        res.json({ "error": err });
      }
      res.json(news);
    });

  }
};


/**
 * Delete a news
 *
 * @param {*} req
 * @param {*} res
 */
const newsDelete = (req, res) => {
  if (req.query && req.query.id) {
    News.findById(req.query.id, function (err, news) {
      if (err) {
        res.status(500);
        console.log('error while queryting the news', err)
        res.json({ error: "News doesnt exist" })
      }
      if (news) {
        news.remove(function (err) {
          if (err) {
            res.status(500).json({ message: "There was an error deleting the news" });
          }
          res.status(204).json({});
        })
      } else {
        res.status(404);
        console.log('error while queryting the news', err)
        res.json({ error: "News doesnt exist" })
      }
    });
  } else {
    res.status(404).json({ error: "You must provide a News ID" });
  }
};

/**
 * Update a news
 *
 * @param {*} req
 * @param {*} res
 */
const newsPatch = (req, res) => {
  if (req.query && req.query.id) {
    News.findById(req.query.id, function (err, news) {
      if (err) {
        res.status(404);
        console.log('error while queryting the news', err)
        res.json({ error: "News doesnt exist" })
      }

      news.titulo = req.body.titulo ? req.body.titulo : task.titulo;
      news.descripcion = req.body.descripcion ? req.body.descripcion : task.descripcion;
      news.link = req.body.link ? req.body.link : task.link;
      news.fecha = req.body.fecha ? req.body.fecha : task.fecha;
      news.id_fuente_noticia = req.body.id_fuente_noticia ? req.body.id_fuente_noticia : task.id_fuente_noticia;
      news.id_usuario = req.body.id_usuario ? req.body.id_usuario : task.id_usuario;
      news.id_categoria = req.body.id_categoria ? req.body.id_categoria : task.id_categoria;

      news.save(function (err) {
        if (err) {
          res.status(422);
          console.log('error while saving the news', err)
          res.json({
            error: 'There was an error saving the news'
          });
        }
        res.status(200);
        res.json(news);
      });
    });
  } else {
    res.status(404);
    res.json({ error: "News doesnt exist" })
  }
};

module.exports = {
  newsGet,
  newsPost,
  newsPatch,
  newsDelete
}