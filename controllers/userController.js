const User = require("../models/userModel");

/**
 * Create a user
 *
 * @param {*} req
 * @param {*} res
 */
const userPost = (req, res) => {
  var user = new User();

  user.nombre = req.body.nombre;
  user.apellidos = req.body.apellidos;
  user.email = req.body.email;
  user.contrasena = req.body.contrasena;
  user.direccion1 = req.body.direccion1;
  user.direccion2 = req.body.direccion2;
  user.pais = req.body.pais;
  user.ciudad = req.body.ciudad;
  user.postal = req.body.postal;
  user.telefono = req.body.telefono;
  user.perfil = req.body.perfil;

  if (user.nombre && user.apellidos && user.email && user.contrasena) {
    user.save(function (err) {
      if (err) {
        res.status(422);
        console.log('error while saving the user', err)
        res.json({
          error: 'There was an error saving the user'
        });
      }
      res.status(201);
      res.header({
        'location': `http://localhost:3000/api/users/?id=${user.id}`
      });
      res.json(user);
    });
  } else {
    res.status(422);
    console.log('error while saving the user')
    res.json({
      error: 'No valid data provided for user'
    });
  }
};

/**
 * Get one or all users
 *
 * @param {*} req
 * @param {*} res
 */
const userGet = (req, res) => {
  if (req.query && req.query.id) {
    User.findById(req.query.id, function (err, user) {
      if (err) {
        res.status(404);
        console.log('error while queryting the user', err)
        res.json({ error: "user doesnt exist" })
      }
      res.json(user);
    });
  } else {
    User.find(function (err, user) {
      if (err) {
        res.status(422);
        res.json({ "error": err });
      }
      res.json(user);
    });

  }
};

/**
 * Delete a user
 *
 * @param {*} req
 * @param {*} res
 */
const userDelete = (req, res) => {
  if (req.query && req.query.id) {
    User.findById(req.query.id, function (err, user) {
      if (err) {
        res.status(500);
        console.log('error while queryting the user', err)
        res.json({ error: "User doesnt exist" })
      }
      if(user) {
        user.remove(function(err){
          if(err) {
            res.status(500).json({message: "There was an error deleting the user"});
          }
          res.status(204).json({});
        })
      } else {
        res.status(404);
        console.log('error while queryting the user', err)
        res.json({ error: "User doesnt exist" })
      }
    });
  } else {
    res.status(404).json({ error: "You must provide a User ID" });
  }
};

/**
 * Update a user
 *
 * @param {*} req
 * @param {*} res
 */
const userPatch = (req, res) => {
  if (req.query && req.query.id) {
    User.findById(req.query.id, function (err, user) {
      if (err) {
        res.status(404);
        console.log('error while queryting the user', err)
        res.json({ error: "User doesnt exist" })
      }

      user.nombre = req.body.nombre ? req.body.nombre : user.nombre;
      user.apellidos = req.body.apellidos ? req.body.apellidos : user.apellidos;
      user.email = req.body.email ? req.body.email : user.email;
      user.contrasena = req.body.contrasena ? req.body.contrasena : user.contrasena;
      user.direccion1 = req.body.direccion1 ? req.body.direccion1 : user.direccion1;
      user.direccion2 = req.body.direccion2 ? req.body.direccion2 : user.direccion2;
      user.pais = req.body.pais ? req.body.pais : user.pais;
      user.ciudad = req.body.ciudad ? req.body.ciudad : user.ciudad;
      user.postal = req.body.postal ? req.body.postal : user.postal;
      user.telefono = req.body.telefono ? req.body.telefono : user.telefono;
      user.perfil = req.body.perfil ? req.body.perfil : user.perfil;

      user.save(function (err) {
        if (err) {
          res.status(422);
          console.log('error while saving the user', err)
          res.json({
            error: 'There was an error saving the user'
          });
        }
        res.status(200);
        res.json(user);
      });
    });
  } else {
    res.status(404);
    res.json({ error: "User doesnt exist" })
  }
};

module.exports = {
  userGet,
  userPost,
  userPatch,
  userDelete
}