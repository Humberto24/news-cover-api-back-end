const Category = require("../models/categoryModel");

/**
 * Create a category
 *
 * @param {*} req
 * @param {*} res
 */
const categoryPost = (req, res) => {
  var category = new Category();

  category.categoria = req.body.categoria;

  if (category.categoria) {
    category.save(function (err) {
      if (err) {
        res.status(422);
        console.log('error while saving the category', err)
        res.json({
          error: 'There was an error saving the category'
        });
      }
      res.status(201);
      res.header({
        'location': `http://localhost:3000/api/categories/?id=${category.id}`
      });
      res.json(category);
    });
  } else {
    res.status(422);
    console.log('error while saving the category')
    res.json({
      error: 'No valid data provided for category'
    });
  }
};

/**
 * Get one or all categories
 *
 * @param {*} req
 * @param {*} res
 */
const categoryGet = (req, res) => {
  if (req.query && req.query.id) {
    Category.findById(req.query.id, function (err, category) {
      if (err) {
        res.status(404);
        console.log('error while queryting the category', err)
        res.json({ error: "Category doesnt exist" })
      }
      res.json(category);
    });
  } else {
    Category.find(function (err, category) {
      if (err) {
        res.status(422);
        res.json({ "error": err });
      }
      res.json(category);
    });

  }
};

/**
 * Delete a category
 *
 * @param {*} req
 * @param {*} res
 */
const categoryDelete = (req, res) => {
  if (req.query && req.query.id) {
    Category.findById(req.query.id, function (err, category) {
      if (err) {
        res.status(500);
        console.log('error while queryting the category', err)
        res.json({ error: "Category doesnt exist" })
      }
      if(category) {
        category.remove(function(err){
          if(err) {
            res.status(500).json({message: "There was an error deleting the category"});
          }
          res.status(204).json({});
        })
      } else {
        res.status(404);
        console.log('error while queryting the category', err)
        res.json({ error: "Category doesnt exist" })
      }
    });
  } else {
    res.status(404).json({ error: "You must provide a Category ID" });
  }
};

/**
 * Updates a task
 *
 * @param {*} req
 * @param {*} res
 */
const categoryPatch = (req, res) => {
  if (req.query && req.query.id) {
    Category.findById(req.query.id, function (err, category) {
      if (err) {
        res.status(404);
        console.log('error while queryting the category', err)
        res.json({ error: "Category doesnt exist" })
      }

      category.categoria = req.body.categoria ? req.body.categoria : task.categoria;

      category.save(function (err) {
        if (err) {
          res.status(422);
          console.log('error while saving the category', err)
          res.json({
            error: 'There was an error saving the category'
          });
        }
        res.status(200);
        res.json(category);
      });
    });
  } else {
    res.status(404);
    res.json({ error: "Category doesnt exist" })
  }
};

module.exports = {
  categoryGet,
  categoryPost,
  categoryPatch,
  categoryDelete
}