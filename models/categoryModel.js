const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const category = new Schema ({
    categoria: {type: String}
});

module.exports = mongoose.model("categories", category);