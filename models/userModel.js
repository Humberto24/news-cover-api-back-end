const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const user = new Schema ({
    nombre: {type: String},
    apellidos: {type: String},
    email: {type: String},
    contrasena: {type: String},
    direccion1: {type: String},
    direccion2: {type: String}, 
    pais: {type: String}, 
    ciudad: {type: String},
    postal: {type: String},
    telefono: {type: String},
    perfil: {type: String}  
});

module.exports = mongoose.model("users", user);