const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const newsSource = new Schema ({
    url: {type: String},
    nombre: {type: String},
    id_categoria: {type: String},
    id_usuario: {type: String}
});

module.exports = mongoose.model("newsSources", newsSource); 